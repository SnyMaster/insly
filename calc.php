<?php

use insly\Form;
use insly\BuilderCalculator;
use insly\ReportCalculator;

define('BASE_PATH', realpath(dirname(__FILE__)));
function insly_autoloader($class)
{
    $filename = BASE_PATH . '/' . str_replace('\\', '/', $class) . '.php';
    include($filename);
}
spl_autoload_register('insly_autoloader');

if (!empty($_POST)) {
    $formData = new Form();
    try {    
        if ($formData->load($_POST)) {

                $calc = (new BuilderCalculator())->setEstimate($formData->getEstimate())
                        ->setTax($formData->getTax())
                        ->setInstalments($formData->getInstalments())
                        ->setBasePrice(11)
                        ->setUserDay($formData->getUserDay())
                        ->setUserHour($formData->getUserHour())
                        ->setCommition(17)
                        ->setExtendPrice(20)
                        ->setExtendPriceDay('Fri')
                        ->setExtendPriceHourFrom(15)
                        ->setExtendPriceHourTo(20)
                        ->getCalculator();
                echo (new ReportCalculator())->renderReport($calc->calculate());
        } else {
            echo '<p class="error-message">' . implode('<br/>', $formData->getErrors()) . '</p>';
        }
    } catch (\Exception $ex) {
        echo '<p class="error-message">' . $ex->getMessage() . '</p>';
    }    
}