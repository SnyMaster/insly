<?php
namespace insly;

interface DataInterface
{
    public function getEstimate();
    public function getTax();
    public function getInstalments();
    public function getUserDay();
    public function getUserHour();
}